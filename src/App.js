import "./App.css";
import { Provider, TitleBar, ResourcePicker } from "@shopify/app-bridge-react";
import { useState, useEffect } from "react";
import { Router } from "@reach/router";
import queryString from "query-string";

const config = {
  apiKey: "ecc949d38f9fc120180968762d9f52b0",
};

function App() {
  return (
    <Router>
      <Page default path="/" />
    </Router>
  );
}

const Page = ({ ...props }) => {
  const [open, setOpen] = useState(false);
  const [shop, setShop] = useState("");
  const primaryAction = { content: "Foo", url: "/foo" };
  const secondaryActions = [{ content: "Bar", url: "/bar" }];
  const actionGroups = [
    { title: "Baz", actions: [{ content: "Baz", url: "/baz" }] },
  ];
  useEffect(() => {
    const params = queryString.parse(props?.location?.search);
    if (params?.shop) {
      setShop(params.shop);
    }
  }, []);

  if (!shop) {
    return <>Loading...</>;
  }

  return (
    <Provider path="/" config={{ ...config, shopOrigin: shop }}>
      <TitleBar
        title="Hello world!"
        primaryAction={primaryAction}
        secondaryActions={secondaryActions}
        actionGroups={actionGroups}
      />
      <button onClick={() => setOpen(true)}>Products</button>
      <ResourcePicker
        resourceType="Product"
        open={open}
        onCancel={() => setOpen(false)}
        onSelection={(selectPayload) => {
          alert(JSON.stringify(selectPayload));
          setOpen(false);
        }}
      />
    </Provider>
  );
};

export default App;
