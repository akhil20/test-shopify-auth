import "../App.css";
import { useAppBridge } from "@shopify/app-bridge-react";
import { Button, Redirect } from "@shopify/app-bridge/actions";

function Content() {
  return <RedirectButton />;
}

function RedirectButton() {
  const app = useAppBridge();
  const redirect = Redirect.create(app);

  const handleClick = () => {
    // Go to {shopUrl}/admin/customers with newContext
    redirect.dispatch(Redirect.Action.ADMIN_PATH, {
      path: "/customers",
      newContext: true,
    });
  };

  return <Button onClick={handleClick}>Activate Lasers for Customers</Button>;
}

export default Content;
